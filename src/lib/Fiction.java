package lib;
import java.util.*;

/**
 * Une fiction se définit par le nom du film, l’année de sa réalisation, le nom du réalisateur et s’il s’agit ou non d’une rediffusion.
 */
public class Fiction extends Emission {

	/**
	 * Nom des réalisateurs d'une fiction, souvent 1, parfois plusieurs.
	 * 
	 */
	private List<String> nomsRealisateurs = new ArrayList<String>();

	/**
	 * Année de réalisation de la fiction.
	 */
	private int anneeRealisation;

	/**
	 * Indicateur de rediffusion.
	 */
	private Boolean rediffusion;

	/**
	 * Constructeur.
	 * 
	 * @param nom 
	 * @param duree 
	 * @param nomCompletRealisateur 
	 * @param anneeRealisation 
	 * @param rediffusion
	 */
	public Fiction(String nom, int duree, String nomCompletRealisateur, int anneeRealisation, Boolean rediffusion) {
		super(nom,duree);
		this.addNomCompletRealisateur(nomCompletRealisateur);
		this.setAnneeRealisation(anneeRealisation);
		this.setRediffusion(rediffusion);
	}

	/**
	 * Ajouter un réalisateur au tableau des réalisateurs de la fiction.
	 * 
	 * @param nom
	 */
	public void addNomCompletRealisateur(String nom) {
		this.nomsRealisateurs.add(nom);	
	}

	/**
	 * Retirer le nom d'un réalisateur du tableau des réalisateurs de la fiction.
	 * 
	 * @param nom
	 */
	public void delNomCompletRealisateur(String nom) {
		for (Iterator<String> iterator = this.nomsRealisateurs.iterator(); iterator.hasNext();) {
			String s =  iterator.next();
			if (s == nom) {
				iterator.remove();
			}       
		}
	}

	/**
	 * Enregistrer l'année de réalisation.
	 * 
	 * @param annee
	 */
	public void setAnneeRealisation(int annee) {
		this.anneeRealisation = annee;
	}

	/**
	 * Enregistrer l'indicateur de rediffusion. 
	 * 
	 * Amélioration possible :
	 * L'état de rediffusion pourrait être automatiquement déterminé en analysant l'historique de la programmation et en mettant à jour les mêmes fictions de manière à refleter dans cette indicateur la diffusion qui fait office d'origine.
	 * 
	 * @param rediffusion
	 */
	public void setRediffusion(Boolean rediffusion) {
		this.rediffusion = rediffusion;
	}

	/**
	 * Obtenir l'année de réalisation de la fiction.
	 * 
	 * @return
	 */
	public int getAnneeRealisation() {
		return this.anneeRealisation;
	}

	/**
	 * Obtenir la valeur de l'indicateur de rediffusion.
	 * 
	 * @return
	 */
	public Boolean getRediffusion() {
		return this.rediffusion;
	}

	/**
	 *
	 * Afficher la liste des réalisateurs de la fiction.
	 * 
	 * @return
	 */
	public List<String> getRealisateurs() {
		return this.nomsRealisateurs;
	}

	/**
	 * Les fictions qui ne sont pas des rediffusion ne se programment qu’en début de soirée, c’est-à-dire qu’à 21h, alors qu’une rediffusion peut se programmer n’importe quand dans la journée.
	 *
	 * @return
	 */
	public Boolean validerContrainte() {

		Boolean res = true;

		if(this.getRediffusion() == false) {
			if((this.getHeureDebut() != -1)&&(this.getHeureDebut() != 21)) {
				res = false;
			}
		}

		return res;
	}

}
