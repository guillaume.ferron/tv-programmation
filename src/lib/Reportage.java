package lib;
import java.util.*;

/**
 * Enfin un reportage est défini par un nom, un thème (fixé parmi les trois thèmes : information, animalier, culturel) et une durée.
 */
public class Reportage extends Emission {

	/**
	 * Thème du reportage.
	 */
	private String theme;

	/**
	 * Constructeur.
	 * 
	 * @param nom 
	 * @param duree 
	 * @param theme
	 */
	public Reportage(String nom, int duree, String theme) {
		super(nom,duree);
		this.setTheme(theme);
	}

	/**
	 * Obtenir le thème du reportage.
	 * 
	 * @return
	 */
	public String getTheme() {
		return this.theme;
	}

	/**
	 * Fixer le thème du reportage.
	 * 
	 * @param theme
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}

	/**
	 * 
	 */
	public enum Theme {
		information,
		animalier,
		culturel
	}

	/**
	 *
	 * Les reportages ne se programment qu’à des heures creuses (14h –18h et 0h-6h) et s’ils ont une durée inférieure, égale à 1 heure.
	 *
	 *
	 * @return
	 */
	public Boolean validerContrainte() {

		Boolean res = true;

		if(!(this.getDuree() <= 1)) {
			res = false;
		}

		if(((this.getHeureDebut()!=-1) &&
				((this.getHeureDebut() >= 14 && this.getHeureFin()<=18)
				||
				(this.getHeureDebut() >= 0 && this.getHeureFin()<=6)))==false) {
			res = false;
		}

		return res;
	}

}
