package lib;
import java.util.*;

/**
 * Une émission de type divertissement dure obligatoirement 2 heures, possède un nom et est systématiquement présentée par un animateur.
 */
public class Divertissement extends Emission {

	/**
	 * Animateur animant l'émission.
	 */
	private Animateur animateur;


	/**
	 * Constructeur. Doit fixer la durée à 2 heures.
	 * 
	 * @param nom 
	 * @param duree 
	 * @param animateur
	 */
	public Divertissement(String nom, Animateur animateur) {
		super(nom,2);
		this.setAnimateur(animateur);
	}

	/**
	 * Enregistrer l'animateur de l'émission de divertissement.
	 * 
	 * @param animateur
	 */
	public void setAnimateur(Animateur animateur) {
		this.animateur = animateur;
	}

	/**
	 * Récupérer l'animateur de l'émission de divertissement.
	 * 
	 * @return
	 */
	public Animateur getAnimateur() {
		return this.animateur;
	}

	/**
	 * *On ne peut programmer les divertissements qu’entre 18h et 23h.*
	 * 
	 * @return
	 */
	public Boolean validerContrainte() {

		Boolean res = true;

		if(this.getDuree() != 2) {
			res= false;
		}

		if ((this.getHeureDebut() == -1) || (!(this.getHeureDebut() >= 18) && !(this.getHeureDebut() > 23-this.getDuree()))) {
			res= false;
		}

		return res;
	}

}
