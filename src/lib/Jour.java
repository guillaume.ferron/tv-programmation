package lib;
import java.util.*;

/**
 * Journée situé dans le temps à une date connue, qui contient une liste de créneaux.
 */
public class Jour {

	/**
	 * Date du jour.
	 */
	private Date date;

	/**
	 * Emissions de la journée. Un tableau d'émission de plusieurs types (divertissement, reportage, fiction...). Polymorphisme.
	 */
	private List<Emission> emissions = new ArrayList<Emission>();

	/**
	 * Constructeur.
	 * 
	 * @param date
	 */
	public Jour(Date date) {
		this.date = date;
	}

	/**
	 * Retourner la date du jour.
	 * 
	 * @return
	 */
	public Date getDate() {
		return this.date;
	}

	/**
	 * Récupérer le tableau des émissions de la journée.
	 * 
	 * @return
	 */
	public List<Emission> getEmissions() {
		return this.emissions;
	}

	/**
	 * Ajouter une émission à la journée.
	 * 
	 * @param emission
	 */
	public void addEmission(Emission emission, int heureDebut) {
		if(this.detecterSuperposition(heureDebut)==true) {
			throw new IllegalStateException("La superposition de deux emissions est impossible");
		}
		emission.setHeureDebut(heureDebut);
		if(emission.validerContrainte()) {
			this.emissions.add(emission);
		} else {
			throw new IllegalStateException("Contraintes non respectées");
		}
	}

	/**
	 * Détecter les conflits d'horaire lorsque 2 émissions risquent de se chevaucher dans le planning quotidien.	 * 
	 * @param emission
	 */
	public Boolean detecterSuperposition(int heureDebut) {
		List<Emission> emissions = this.getEmissions();
		Boolean res = false;
		for(int i=0; i<emissions.size(); i++) {
			if(heureDebut>=emissions.get(i).getHeureDebut() && heureDebut<=emissions.get(i).getHeureFin()) {
				res = true;
			}
		}
		return res;
	}

	/**
	 * Supprimer une émission de la journée.
	 * 
	 * @param emission
	 */
	public void delEmission(Emission emission) {
		this.emissions.remove(emission);
	}

}
