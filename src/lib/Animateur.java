package lib;
import java.util.*;

/**
 * Personne animant les émissions de type Divertissement.
 */
public class Animateur {

	/**
	 * Nom de famille de l'animateur.
	 */
	private String nom;

	/**
	 * Prénom usuel de l'animateur.
	 */
	private String prenom;

	/**
	 * Constructeur.
	 * 
	 * @param nom 
	 * @param prenom
	 */
	public Animateur(String nom, String prenom) {
		this.setNom(nom);
		this.setPrenom(prenom);
	}

	/**
	 * Enregistrer le nom de l'animateur en cours. 
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Enregistrer le prénom de l'animateur en cours. 
	 * 
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Obtenir le nom de l'animateur.
	 * 
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Obtenir le prénom de l'animateur.
	 * 
	 * @return
	 */
	public String getPrenom() {
		return this.prenom;
	}

}
