package lib;
import java.util.*;

/**
 * Emission de télévision.
 */
public abstract class Emission {

	/**
	 * Nom d'une émission. Le premier caractère doit être en majuscule accentué.
	 */
	private String nom;

	/**
	 * Heure du début de la programmation de l'émission dans un Jour. Donnée par le constructeur via le setter. Exprimée en heure arrondie : *Pour simplifier le problème, toutes les heures ou les durées sont représentées par des valeurs entières (13h, 15h, 22h...).
	 * *
	 */
	private int heureDebut = -1;

	/**
	 * Heure de fin de la programmation de l'émission dans un Jour. Calculée par le constructeur via setHeureFin. Exprimée en heure arrondie : *Pour simplifier le problème, toutes les heures ou les durées sont représentées par des valeurs entières (13h, 15h, 22h...).
	 * *
	 */
	private int heureFin = -1; 
	/**
	 * Durée de l'émission. Donnée par le constructeur via le setter.
	 */
	private int duree = -1;


	/**
	 * Constructeur. 
	 * 
	 * @param nom 
	 * @param duree
	 */
	public Emission(String nom, int duree) {
		this.setNom(nom);
		this.setDuree(duree);
		this.setHeureDebut(heureDebut);
	}

	/**
	 * Enregistre le nom de l'Emission. 
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Enregistre la durée de l'émission. 
	 * 
	 * @param duree
	 */
	public void setDuree(int duree) {
		this.duree = duree;
	}

	/**
	 * Calcule et enregistre l'heure de fin de diffusion de l'émission. Exprimée en heure arrondie : *Pour simplifier le problème, toutes les heures ou les durées sont représentées par des valeurs entières (13h, 15h, 22h...).*
	 * 
	 */
	public void setHeureFin() {
		if( this.duree > -1) {
			this.heureFin = this.heureDebut + this.duree;
		} else {
			throw(new IllegalStateException("Emission duration is missing"));
		}
	}

	/**
	 * Retourne le nom de l'émission.
	 * 
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Retourne la durée de l'émission.
	 * 
	 * @return
	 */
	public int getDuree() {
		return this.duree;
	}

	/**
	 * Retourne l'heure de début de l'émission.Exprimée en heure arrondie : Pour simplifier le problème, toutes les heures ou les durées sont représentées par des valeurs entières (13h, 15h, 22h...).
	 * 
	 * @return
	 */
	public int getHeureDebut() {
		return this.heureDebut;
	}

	/**
	 * Retourne l'heure de fin de l'émission.Exprimée en heure arrondie : *Pour simplifier le problème, toutes les heures ou les durées sont représentées par des valeurs entières (13h, 15h, 22h...).*
	 * 
	 * @return
	 */
	public int getHeureFin() {
		return this.heureFin;
	}

	/**
	 * Enregistre l'heure de début de l'émission. Exprimée en heure arrondie : *Pour simplifier le problème, toutes les heures ou les durées sont représentées par des valeurs entières (13h, 15h, 22h...).*
	 * 
	 * @param heure
	 */
	public void setHeureDebut(int heure) {
		this.heureDebut = heure;
		this.setHeureFin();
	}

	/**
	 * A pour but d'être implémenté dans toutes les classes filles. Si elles ne surchargent avec leur propre test, la fonction retourne TRUE.
	 * 
	 * @return
	 */
	public Boolean validerContrainte() {
		return true;
	}

}
