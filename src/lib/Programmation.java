package lib;
import java.util.*;

/**
 * Calendrier qui enregistre des jours de diffusion à des dates précises.
 */
public class Programmation {

	/**
	 * Attribut qui stocke le calendrier de la classe. Il peut être rempli ou vide.
	 */
	private List<Jour> calendrier = new ArrayList<Jour>();

	/**
	 * Constructeur.
	 * public Programmation() {}
	 */

	/**
	 * Ajouter au calendrier un jour préalablement instancié.
	 * 
	 * @param jour
	 */
	public void addJour(Jour jour) {
		this.calendrier.add(jour);
	}

	/**
	 * Supprimer  un jour  précis du calendrier.
	 * 
	 * @param jour
	 */
	public void delJour(Jour jour) {
		this.calendrier.remove(jour);
	}

	/**
	 * Récupérer un jour précis dans le calendrier à partir d'une date.
	 * 
	 * @param date 
	 * @return
	 */
	public Jour getJour(Date date) {

		List<Jour> calendrier = this.getCalendrier();

		Jour jour = new Jour(date);//TODO improve

		for (int i = 0; i < calendrier.size(); i++) {
			if(calendrier.get(i).getDate() == date) {
				jour = calendrier.get(i);
				break;
			}
		}

		return jour;
	}

	/**
	 * Obtenir le calendrier complet.
	 * 
	 * @return
	 */
	public List<Jour> getCalendrier() {
		return this.calendrier;
	}

}
