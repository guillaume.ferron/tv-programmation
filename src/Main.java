import lib.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

class Main {

	private static Boolean detail = true;

	public static void main(String[] args) {

		Main.test();
		MenuPrincipal();

	}

	public static void warning(String message) {
		System.out.println("\n/!\\ "+message+"\n");
	}

	public static void MenuAjouter(String titre,String options2_nom,String options2_date,String options2_type,String options2_heure,Boolean options2_heure_set){
		int choice2 = -1; 
		ArrayList<String> options2 = new ArrayList<String>();;
		options2.add("Nom de l'émission [min. 3 char.] : "+options2_nom);
		options2.add("Date de l'émission [dd/MM/yyyy] : "+options2_date);
		options2.add("Type de l'émission [Divertissement|Fiction|Reportage] : "+options2_type);
		options2.add("Heure de début de l'émission : "+options2_heure);
		options2.add("Valider");
		options2.add("Retour");

		choice2 = Main.menu(titre,options2);
		Scanner scanner = new Scanner(System.in);
		switch(choice2) {
			case 1: options2_nom = scanner.nextLine();
				if(options2_nom.length() > 3) {
					options2_nom = options2_nom.substring(0, 1).toUpperCase() + 
						options2_nom.substring(1);
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} else {
					Main.warning("Le nom est trop court, plus de 3 caractères sont requis");
					options2_nom = "";
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				}
				break;
			case 2: options2_date = scanner.nextLine();
				try {
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					formatter.setLenient(false);
					Date date1 = formatter.parse(options2_date);
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} catch (Exception e) {
					Main.warning("Mauvaise valeur pour date");
					options2_date = "";
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} 
				break;
			case 3: options2_type = scanner.nextLine();
				if(options2_type.toUpperCase().equals("DIVERTISSEMENT")) {
					options2_type = options2_type.substring(0, 1).toUpperCase() + 
						options2_type.substring(1);
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} else if(options2_type.toUpperCase().equals("FICTION")) {
					options2_type = options2_type.substring(0, 1).toUpperCase() + 
						options2_type.substring(1);
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} else if(options2_type.toUpperCase().equals("REPORTAGE")) {
					options2_type = options2_type.substring(0, 1).toUpperCase() + 
						options2_type.substring(1);
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} else {
					Main.warning("Mauvaise valeur pour type");
					options2_type = "";
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				}
				break;
			case 4: options2_heure = scanner.nextLine();
				try {
					int test_heure = Integer.parseInt(options2_heure);
					if(test_heure>=0 && test_heure<=23) {
						options2_heure_set = true;
						MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
					} else {
						Main.warning("Mauvaise valeur pour heure de début");
						options2_heure = "";
						options2_heure_set = false;
						MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
					}
				} catch(Exception e) {
					Main.warning("Mauvaise valeur pour heure de début");
					options2_heure = "";
					options2_heure_set = false;
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				}
				break;
			case 5: 
				if(options2_nom == "" || options2_date == "" || options2_type == "" || options2_heure_set == false) {
					Main.warning("Vous devez d'abord remplir toutes les valeurs!");
					MenuAjouter(titre,options2_nom,options2_date,options2_type,options2_heure,options2_heure_set);
				} else {

					String[] date_final = options2_date.split("/",0);
					Jour nouveauJour = new Jour(new GregorianCalendar(
								Integer.parseInt(date_final[2]), 
								Integer.parseInt(date_final[1]), 
								Integer.parseInt(date_final[0])).getTime());

					if(options2_type.toUpperCase().equals("DIVERTISSEMENT")) {

						System.out.println("Entrez le prenom de l'animateur : ");
						String options2_animateurPrenom = "";
						String m = "";
						while(m.equals("")) {
							m = scanner.nextLine();
							options2_animateurPrenom = m;
						}

						System.out.println("Entrez le nom de l'animateur : ");
						String options2_animateurNom = "";
						String k = "";
						while(k.equals("")) {
							k = scanner.nextLine();
							options2_animateurNom = k;
						}

						Animateur options2_animateur = new Animateur(options2_animateurNom,options2_animateurPrenom);

						Divertissement nouveauDiv = new Divertissement(options2_nom,options2_animateur);

						Emission nouvelleEmission = (Emission) nouveauDiv;
						nouveauJour.addEmission(nouvelleEmission,Integer.parseInt(options2_heure));

					} else if(options2_type.toUpperCase().equals("FICTION")) {

						System.out.println("Entrez la durée de la fiction : ");
						String options2_duree = "";
						String w = "";
						while(w.equals("")) {
							w = scanner.nextLine();
							options2_duree = w;
						}

						System.out.println("Entrez l'année de réalisation : ");
						String options2_anneereal = "";
						String m = "";
						while(m.equals("")) {
							m = scanner.nextLine();
							options2_anneereal = m;
						}

						System.out.println("Entrez le prénom et nom d'un réalisateur (tapez 'next' pour passer à la suite) : ");
						ArrayList<String> options2_rea = new ArrayList<String>();
						String k = "";
						while(options2_rea.size()==0 || !k.equals("next")) {
							k = scanner.nextLine();
							if(!k.equals("") && !k.equals("next")) options2_rea.add(k);
						}

						System.out.println("S'agit-il d'un rediffusion? [oui/non]");
						String options2_redif = "";
						String l = "";
						while(!l.equals("oui") && !l.equals("non")) {
							l = scanner.nextLine();
							options2_redif = l;
						}

						Fiction nouveauFic = new Fiction(options2_nom,
								Integer.parseInt(options2_duree),
								options2_rea.get(0),
								Integer.parseInt(options2_anneereal),
								Boolean.parseBoolean(options2_redif));
						for(int v=1; v<options2_rea.size();v++) {
							nouveauFic.addNomCompletRealisateur(options2_rea.get(v));
						}

						Emission nouvelleEmission = (Emission) nouveauFic;
						nouveauJour.addEmission(nouvelleEmission,Integer.parseInt(options2_heure));

					} else if(options2_type.toUpperCase().equals("REPORTAGE")) {
						
						System.out.println("Entrez la durée du reportage : ");
						String options2_duree = "";
						String w = "";
						while(w.equals("")) {
							w = scanner.nextLine();
							options2_duree = w;
						}

						System.out.println("Thème : ");
						String options2_theme = "";
						String l = "";
						while(l.equals("")) {
							l = scanner.nextLine();
							options2_theme = l;
						}

						detail("MESSAGE 1");
						Reportage nouveauRep = new Reportage(options2_nom,
								Integer.parseInt(options2_duree),
								options2_theme);

						detail("MESSAGE 2");
						Emission nouvelleEmission = (Emission) nouveauRep;
						
						detail("MESSAGE 3");//bug non resolu 14042019
						nouveauJour.addEmission(nouvelleEmission,Integer.parseInt(options2_heure));

						detail("MESSAGE 4");
					}

					detail("MESSAGE 5");
					detail(nouveauJour.getEmissions().size());
					detail("MESSAGE 6");

					MenuPrincipal();

					return;

					//int[] date_final = {
					//	Integer.parseInt(options2_date.split("/",0)[0]),
					//	Integer.parseInt(options2_date.split("/",0)[1]),
					//	Integer.parseInt(options2_date.split("/",0)[2])
					//};

					//Jour nouveauJour = new Jour(new GregorianCalendar(date_final[2], date_final[1], date_final[0]).getTime());
					//detail("LOLO Entrée ajoutée avec succès!");
					//List<Emission> emi = nouveauJour.getEmissions();
					//System.out.println(emi.size());

					//Emission nouvelleEmission = (Emission) nouveauDiv;
					//nouveauJour.addEmission(nouvelleEmission,10);

					//MenuPrincipal();
				}
				break;
			case 6: MenuPrincipal();
				break;
		}
	}

	public static void MenuPrincipal() {
		ArrayList<String> options1 = new ArrayList<String>();;
		options1.add("Ajouter une émission");
		options1.add("Supprimer une émission");
		options1.add("Consulter la programmation quotidienne");
		options1.add("Consulter le planning global");

		int choice1 = -1;

		while(choice1 != 0) {
		choice1 = Main.menu("Menu Principal",options1);

		switch (choice1) {
			case 1:
				MenuAjouter(options1.get(0),"","","","",false);
				break;
			case 2:
				// TODO
				break;
			case 3:
				// TODO
				break;
			case 4:
				// TODO
				break;
			case 0:
				System.out.println("Bye.");
				System.exit(0);
				break;

		}
		}
	}

	public static int menu(String titre, ArrayList<String> options) {

			int choice = -1;

			System.out.println(titre.toUpperCase());

			for(int i=0; i<options.size();i++) {
				System.out.println(Integer.toString(i+1)+" - "+options.get(i));
			}

			System.out.println("0 - Exit");

				Scanner scanner = new Scanner(System.in);
				try {
					choice = scanner.nextInt();
				} catch(Exception e) {
					choice = 0;
				}
			return choice;

	}

	public static void test() {
		try{
			sayLoud("\nDébut des tests");

			say("Test de la classe Animateur");
			detail("Instanciation de la classe");
			Animateur animateur = new Animateur("Y","Z");
			detail("Enregistrement du nom");
			animateur.setNom("A");
			detail("Enregistrement du prenom");
			animateur.setPrenom("B");
			detail("Nom de l'animateur : "+animateur.getNom());
			detail("Prénom de l'animateur : "+animateur.getPrenom());
			if(animateur.getNom() != "A") {
				failed();
			}
			if(animateur.getPrenom() != "B") {
				failed();
			}
			pass();

			say("Test de la classe Divertissement");
			detail("Instanciation de la classe");
			Divertissement divertissement = new Divertissement("C",animateur);
			detail("Enregistrement de l'animateur");
			animateur = new Animateur("D","E");
			divertissement.setAnimateur(animateur);
			animateur = divertissement.getAnimateur();
			detail("Nouvel animateur : "+animateur.getPrenom()+" "+animateur.getNom());
			if(animateur.getNom() != "D") {
				failed();
			}
			if(animateur.getPrenom() != "E") {
				failed();
			}
			pass();

			say("Test de la classe Fiction");
			detail("Instanciation de la classe");
			Fiction fiction = new Fiction("S",3,"D L",2001,true);
			if(fiction.getRealisateurs().get(0) != "D L") {
				failed();
			}
			detail("Ajout d'un réalisateur");
			fiction.addNomCompletRealisateur("E G");
			if(fiction.getRealisateurs().get(0) != "D L" 
					&& fiction.getRealisateurs().get(1) != "E G") {
				failed();
			}
			detail("Suppression d'un réalisateur");
			fiction.delNomCompletRealisateur("D L");
			if(fiction.getRealisateurs().get(0) != "E G") { 
				failed();
			}
			detail("Ajout de l'année de réalisation");
			fiction.setAnneeRealisation(2002);
			if(fiction.getAnneeRealisation() != 2002) {
				failed();
			}
			detail("Enregistrement du drapeau de rediffusion");
			Boolean saved = fiction.getRediffusion();
			fiction.setRediffusion(true);
			if(fiction.getRediffusion() != true) {
				failed();
			}
			fiction.setRediffusion(false);
			if(fiction.getRediffusion() != false) {
				failed();
			}
			fiction.setRediffusion(saved);
			pass();

			say("Test de la classe Reportage");
			detail("Instanciation de la classe");
			Reportage reportage = new Reportage("R",4,"N");
			detail("Enregistrement du thème");
			reportage.setTheme("F");
			detail("Obtention du thème");
			if(reportage.getTheme() != "F") {
				failed();
			}
			pass();

			say("Test de la classe Emission");
			detail("Accès des classes filles à la classe parent");
			Emission[] emissions = {divertissement, fiction, reportage};
			String[] nomsEmissions = {"Divertissement","Fiction","Reportage"};
			int[] dureesEmissions = {2,3,1};
			int[] debutEmissions = {18,17,3};
			for (int i = 0; i < emissions.length; i++) { 
				Emission x = emissions[i]; 
				String y = nomsEmissions[i]; 
				int z = dureesEmissions[i]; 
				int a = debutEmissions[i]; 
				detail("Enregistrer nom de l'émission de type "+y);
				x.setNom("G");
				detail("Enregistrer durée de l'émission de type "+y);
				x.setDuree(z);
				detail("Calculer heure de fin de l'émission de type "+y);
				x.setHeureDebut(a);
				detail("Nom du l'émission de type "+y+" : "+x.getNom());
				if(x.getNom() != "G") {
					failed();
				}
				detail("Durée de l'émission de type "+y+" : "+x.getDuree());
				if(x.getDuree() != z) {
					failed();
				}
				if(x.getHeureDebut() != a) {
					detail("Heure de début de l'émission de type "+y+" : "+x.getHeureDebut());
					failed();
				}
				if(x.getHeureFin() != a+z) {
					detail("Heure de fin de l'émission de type "+y+" : "+x.getHeureFin());
					failed();
				}
			} 
			pass();

			say("Test de la classe Jour");
			detail("Instanciation de la classe");
			Date date = new GregorianCalendar(2019, Calendar.APRIL, 14).getTime();
			Jour jour = new Jour(date);
			detail("Récupération de la date du jour");
			if(jour.getDate() != date) {
				failed();
			}
			detail("Ajout d'une émission de type Divertissement");
			jour.addEmission(divertissement,debutEmissions[0]);
			detail("Ajout d'une émission de type Fiction");
			jour.addEmission(fiction,debutEmissions[1]);
			detail("Ajout d'une émission de type Reportage");
			jour.addEmission(reportage,debutEmissions[2]);
			detail("Récupération des émissions de la journée");
			List<Emission> planning = jour.getEmissions();
			detail("Les émissions sont bien des émissions");
			if(!(planning.get(0) instanceof Emission)) {
				failed();
			}
			detail("Les divertissements sont bien des divertissements");
			Divertissement div = (Divertissement) planning.get(0);
			if(!(div instanceof Divertissement)) {
				failed();
			}
			detail("Les fictions sont bien des fictions");
			Fiction fict = (Fiction) planning.get(1);
			if(!(fict instanceof Fiction)) {
				failed();
			}
			detail("Les reportages sont bien des reportages");
			Reportage rep = (Reportage) planning.get(2);
			if(!(rep instanceof Reportage)) {
				failed();
			}
			detail("Suppression d'une émission de type Reportage");
			Reportage repToDel = (Reportage) planning.get(2);
			jour.delEmission(repToDel);
			if(planning.size() != 2) {
				failed();
			}
			detail("Suppression d'une émission de type Fiction");
			Fiction ficToDel = (Fiction) planning.get(1);
			jour.delEmission(ficToDel);
			if(planning.size() != 1) {
				failed();
			}
			detail("Suppression d'une émission de type Divertissement");
			Divertissement divToDel = (Divertissement) planning.get(0);
			jour.delEmission(divToDel);
			if(planning.size() != 0) {
				failed();
			}
			pass();

			say("Test de la classe Programmation");
			detail("Instanciation de la classe");
			Programmation programmation = new Programmation();
			detail("Ajouter un jour");
			programmation.addJour(jour);
			detail("Retirer un jour");
			programmation.delJour(jour);
			detail("Récupérer un jour depuis le calendrier");
			Date date2 = new GregorianCalendar(2019, Calendar.APRIL, 14).getTime();
			Jour j = programmation.getJour(date2);
			if(!j.getDate().equals(date)) {
				failed();
			}
			detail("Récupérer le calendrier");
			programmation.getCalendrier();
			pass();

			sayLoud("Fin des tests.\n");

		} catch(Exception e) {
			failed();
		}

	}

	public static void say(String x) {
		System.out.println(x);
	}

	public static void sayLoud(String x) {
		System.out.println(x.toUpperCase());
	}

	public static void detail(String x) {
		if(Main.detail == true) Main.say(" --> "+x);
	}

	public static void detail(int x) {
		if(Main.detail == true) System.out.println(" --> "+x);
	}

	public static void detail(List<Emission> x) {
		if(Main.detail == true) System.out.println(" --> "+x);
	}
	
	public static void detail(Date x) {
		if(Main.detail == true) System.out.println(" --> "+x);
	}

	public static void pass() {
		System.out.println("...Success");
	}

	public static void failed() {
		System.out.println("...Failure");
		throw new UnknownError();
	}

}
