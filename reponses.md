# Tv-programmation-pro

1) Proposer une solution pour représenter toutes les émissions possibles. Donner pour chaque classe, la liste de ses attributs et les paramètres de ses constructeurs.

### FAIT

2) Implanter cette solution en Java et tester tout d’abord les classes que vous avez imaginées en instanciant différents objets de votre choix pour chacune de celles-ci.

### FAIT

3) La programmation d’une émission dans la journée dépend du type d’émission mais se traduit par le fait de lui fixer une heure de début de diffusion et de calculer l’heure de fin.

    a. Les divertissements durent systématiquement 2 heures, mais on ne peut les programmer qu’entre 18h et 23h.

	### PRIS EN COMPTE

    b. Les fictions qui ne sont pas des rediffusion ne se programment qu’en début de soirée, c’est-à-dire qu’à 21h, alors qu’une rediffusion peut se programmer n’importe quand dans la journée.

	### PRIS EN COMPTE

    c. Enfin, les reportages ne se programment qu’à des heures creuses (14h –18h et 0h-6h) et s’ils ont une durée inférieure, égale à 1 heure.

	### PRIS EN COMPTE

Ajouter à chaque classe deux attributs heureDebut et heureFin initialisés à -1 (### FAIT), ainsi qu’une méthode prenant en paramètre une heure de début, qui :

    - décide si il est possible ou non de programmer l’émission à l’heure de début passée en paramètre,

	### FAIT

    - si c’est le cas modifie les attributs correspondant aux horaires de diffusion

	### FAIT

    - renvoie true ou false selon si la programmation est possible ou non.

	### FAIT

4) Définissez enfin un programme télé comme un ensemble fini d’émissions hétérogènes que vous remplirez d’émission de votre choix, programmés à une heure de votre choix. Décrire puis implémenter les algorithmes vous permettant :

    a. Afficher la liste des émissions programmées dans la journée

	### À FAIRE

    b. Tester s’il y a une superposition de programmation

	### FAIT

    c. Afficher heure par heure les émissions programmées et vérifier que tous les créneaux horaires ont bien été remplis.

	### À FAIRE

